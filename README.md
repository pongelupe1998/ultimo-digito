# Dígito Único

Esse projeto foi gerado utilizando [Spring Initializr](https://start.spring.io/), utilizando Apache Maven e Java 11.

Para executar a aplicação execute o comando `mvn spring-boot:run`

Para executar os testes da aplicação execute o comando `mvn test`
