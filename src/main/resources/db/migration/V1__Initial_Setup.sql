CREATE TABLE user (
    user_id VARCHAR (36) CONSTRAINT user_id_pkey PRIMARY KEY,
    name VARCHAR (70) NOT NULL,
    email VARCHAR (50) NOT NULL
);

CREATE TABLE calculation (
	calculation_id VARCHAR(36) CONSTRAINT calculation_id_pkey PRIMARY KEY,
	n CLOB NOT NULL,
	k INT NOT NULL,
	result INT,
	user_id VARCHAR(36),
	FOREIGN KEY (user_id) REFERENCES user(user_id)
);
