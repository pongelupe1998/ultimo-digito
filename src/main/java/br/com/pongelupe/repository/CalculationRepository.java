package br.com.pongelupe.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.pongelupe.entity.Calculation;

@Repository
public interface CalculationRepository extends JpaRepository<Calculation, String> {

	@Transactional(readOnly = true)
	@Query("SELECT c FROM Calculation c WHERE c.userId = :userId")
	List<Calculation> findAllByUserId(@Param("userId") String userId);

	@Transactional(readOnly = false)
	long deleteByUserId(String userId);
	
}
