package br.com.pongelupe.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.pongelupe.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, String> {

	@Transactional
	@Modifying
	@Query("UPDATE User u SET u.publicKey = :publicKey WHERE u.id = :userId")
	void updateUserPublicKey(@Param("publicKey") String publicKey, @Param("userId") String userId);

}
