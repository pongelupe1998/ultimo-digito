package br.com.pongelupe.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.validation.Validation;
import javax.validation.Validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.pongelupe.dto.CalculationDTO;
import br.com.pongelupe.dto.UserDTO;
import br.com.pongelupe.repository.CalculationRepository;
import br.com.pongelupe.repository.UserRepository;

@Service
public class UserService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private CalculationRepository calculationRepository;
	
	@Autowired
	private CypherService cyperService;
	
	public Optional<UserDTO> createUser(UserDTO user) {
		return Optional.of(new UserDTO(userRepository.save(user.toEntity())));
	}

	public Optional<UserDTO> readUser(String userId) {
		return Optional.ofNullable(userRepository.findById(userId).map(UserDTO::new).orElse(null));
	}

	public Optional<UserDTO> updateUser(String userId, UserDTO user) {
		return Optional.ofNullable(userRepository.findById(userId)
				.map(u -> {
			Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
			if (validator.validateProperty(user, "name").isEmpty()) {
				u.setName(user.getName());
			}

			if (validator.validateProperty(user, "email").isEmpty()) {
				u.setEmail(user.getEmail());
			}

			return u;
		}).map(userRepository::save).map(UserDTO::new).orElse(null));
	}

	public Optional<String> deleteUser(String userId) {
		if (userRepository.existsById(userId)) {
			calculationRepository.deleteByUserId(userId);
			userRepository.deleteById(userId);
			return Optional.of(userId);
		} else {
			return Optional.empty();
		}
	}


	public List<CalculationDTO> readCalculationsByUser(String userId) {
		return calculationRepository.findAllByUserId(userId)
				.stream()
				.map(CalculationDTO::new)
				.collect(Collectors.toList());
	}

	public void addPublicKeyToUser(String userId, String publicKey) {
		Optional<UserDTO> readUser = readUser(userId);
		readUser.ifPresent(u -> {
			userRepository.updateUserPublicKey(publicKey, userId);
			
			//cypher fields
			u.setEmail(cyperService.encrypt(u.getEmail(), publicKey));
			u.setName(cyperService.encrypt(u.getName(), publicKey));
			
			updateUser(userId, u);
		});
		
	}

}
