package br.com.pongelupe.service;

import java.security.KeyFactory;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

import javax.crypto.Cipher;

import org.springframework.stereotype.Service;

@Service
public class CypherService {

	private static final String ALGORITHM = "RSA";

	public String encrypt(String text, String key) {

		if (key != null && text != null) {
			try {
				var byteKey = Base64.getDecoder().decode(key.getBytes());
				var x509publicKey = new X509EncodedKeySpec(byteKey);

				var publicKey = KeyFactory.getInstance(ALGORITHM).generatePublic(x509publicKey);
				var cipher = Cipher.getInstance(ALGORITHM);
				cipher.init(Cipher.ENCRYPT_MODE, publicKey);

				return Base64.getEncoder().encodeToString(cipher.doFinal(text.getBytes()));
			} catch (Exception e) {
				return text;
			}
		}

		return text;
	}

}
