package br.com.pongelupe.service;

import java.math.BigInteger;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.function.UnaryOperator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Service;

import br.com.pongelupe.dto.CalculationDTO;
import br.com.pongelupe.repository.CalculationRepository;
import br.com.pongelupe.repository.UserRepository;

@Service
public class CalculationService {

	@Autowired
	private CalculationRepository calculationRepository;

	@Autowired
	private UserRepository userRepository;
	
	private static final Map<Pair<String, Integer>, Integer> CACHE_RESULTS = Collections.synchronizedMap(new LinkedHashMap<Pair<String, Integer>, Integer>() {

		private static final long serialVersionUID = -3674517864477544908L;
		
		private static final int CACHE_MAX_SIZE = 10;
		
		@Override
        protected boolean removeEldestEntry(final Map.Entry<Pair<String, Integer>, Integer> eldest) {
            return size() > CACHE_MAX_SIZE;
        }
		
	}); 

	public CalculationDTO digitoUnico(CalculationDTO calculationDTO) {
		calculationDTO.setResult(digitoUnico(calculationDTO.getN(), calculationDTO.getK()));
		var entity = calculationDTO.toEntity();

		if (calculationDTO.getUserId() != null && userRepository.existsById(calculationDTO.getUserId())) {
			entity.setUser(userRepository.getOne(calculationDTO.getUserId()));
		}

		return new CalculationDTO(calculationRepository.save(entity));
	}

	public Integer digitoUnico(String n, Integer k) {
		return CACHE_RESULTS.computeIfAbsent(Pair.of(n, k), p -> {
			
			UnaryOperator<String> digitoUnico = d -> d.chars().mapToObj(i -> (char) i).map(Character::getNumericValue)
					.map(BigInteger::valueOf).reduce(BigInteger.ZERO, (subtotal, element) -> subtotal.add(element))
					.toString();

			var sum = BigInteger.valueOf(Long.parseLong(digitoUnico.apply(n))).multiply(BigInteger.valueOf(k)).toString();

			while (sum.length() != 1) {
				sum = digitoUnico.apply(sum);
			}

			return Integer.parseInt(sum);
		});
		
	}

}
