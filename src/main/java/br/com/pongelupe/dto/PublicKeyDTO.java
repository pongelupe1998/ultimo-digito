package br.com.pongelupe.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class PublicKeyDTO {

	private String publicKey; 
	
}
