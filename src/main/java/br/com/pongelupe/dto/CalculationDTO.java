package br.com.pongelupe.dto;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import com.fasterxml.jackson.annotation.JsonIgnore;

import br.com.pongelupe.entity.Calculation;
import br.com.pongelupe.entity.User;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CalculationDTO {

	@JsonIgnore
	private String calculationId;

	@NotBlank
	@Length(min = 1)
	private String n;

	@NotNull
	@Max(100000)
	@Min(1)
	private Integer k;

	private Integer result;

	private String userId;

	public CalculationDTO(Calculation calculation) {
		this.calculationId = calculation.getCalculationId();
		this.n = calculation.getN();
		this.k = calculation.getK();
		this.result = calculation.getResult();
		this.userId = calculation.getUserId();
	}

	public Calculation toEntity() {
		var calculation = new Calculation();
		calculation.setCalculationId(calculationId);
		calculation.setK(k);
		calculation.setN(n);
		calculation.setResult(result);
		if (userId != null) {
			calculation.setUser(new User(userId));
		}

		return calculation;
	}

}
