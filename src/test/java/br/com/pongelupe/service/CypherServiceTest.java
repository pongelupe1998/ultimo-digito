package br.com.pongelupe.service;

import static org.junit.Assert.*;

import org.junit.Rule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.mockito.quality.Strictness;

public class CypherServiceTest {

	@Rule
	public MockitoRule mockitoRule = MockitoJUnit.rule().strictness(Strictness.STRICT_STUBS);

	@InjectMocks
	private CypherService cypherService;

	@Test
	public void shouldEncrypt() {
		// given
		var text = "SOME TEXT";
		var publicKey = "MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBAJQ7BZ66KZfKsbzIxFrrPgFzLSW2LeL+" + 
				"+d+HsZFDBmWhnQPYHQXQUV4EVnjxtVsaJD8O5+xp0tGVrRJEWW3sBvsCAwEAAQ=="; // SOME RAMDON PUBLIC KEY (512 bit)

		// when
		var encryptedTExt = cypherService.encrypt(text, publicKey);

		// Assert
		assertNotEquals(text, encryptedTExt);
	}
	
	@Test
	public void shouldNotEncrypt() {
		// given
		var text = "SOME TEXT";
		var wrongPublicKey = "FOR SURE NOT A PUBLIC KEY";
		
		// when
		var encryptedTExt = cypherService.encrypt(text, wrongPublicKey);
		
		// Assert
		assertEquals(text, encryptedTExt);
	}

}
