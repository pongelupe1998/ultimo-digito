package br.com.pongelupe.service;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Optional;

import org.junit.Rule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.mockito.quality.Strictness;

import br.com.pongelupe.dto.UserDTO;
import br.com.pongelupe.entity.Calculation;
import br.com.pongelupe.entity.User;
import br.com.pongelupe.repository.CalculationRepository;
import br.com.pongelupe.repository.UserRepository;

public class UserServiceTest {

	@Rule
	public MockitoRule mockitoRule = MockitoJUnit.rule().strictness(Strictness.STRICT_STUBS);

	@InjectMocks
	private UserService userService;

	@Mock
	private UserRepository userRepository;
	
	@Mock
	private CalculationRepository calculationRepository;

	@Test
	public void shouldCreateUser() {
		// given
		var userDTO = mock(UserDTO.class);
		var mockedRequestUser = mock(User.class);
		var savedUser = mock(User.class);
		when(userDTO.toEntity()).thenReturn(mockedRequestUser);
		when(userRepository.save(mockedRequestUser)).thenReturn(savedUser);

		// when
		var realUser = userService.createUser(userDTO);

		// Assert
		assertTrue(realUser.isPresent());
		assertEquals(savedUser.getUserId(), realUser.get().getUserId());
		verify(userRepository, times(1)).save(userDTO.toEntity());
	}

	@Test
	public void shouldReadUserExists() {
		// given
		var userIdMocked = "FAKE-ID";
		var userMocked = new User();
		userMocked.setUserId(userIdMocked);
		userMocked.setCalculations(Arrays.asList(new Calculation()));
		when(userRepository.findById(userIdMocked)).thenReturn(Optional.of(userMocked));
		
		// when
		var realUser = userService.readUser(userIdMocked);
		
		// Assert
		assertTrue(realUser.isPresent());
		assertEquals(userMocked.getUserId(), realUser.get().getUserId());
	}

	@Test
	public void shouldReadUserNotExists() {
		// given
		var userIdMocked = "FAKE-ID";
		var userMocked = mock(User.class);
		userMocked.setUserId(userIdMocked);
		when(userRepository.findById(userIdMocked)).thenReturn(Optional.empty());

		// when
		var realUser = userService.readUser(userIdMocked);

		// Assert
		assertTrue(realUser.isEmpty());
	}

	@Test
	public void shouldUpdateUserExists() {
		// given
		var userIdMocked = "FAKE-ID";
		var userMocked = new User();
		userMocked.setUserId(userIdMocked);
		var emailUpdated = "EMAIL@NEW.COM";
		userMocked.setEmail(emailUpdated);

		var userDTO = new UserDTO();
		userDTO.setEmail(emailUpdated);

		when(userRepository.findById(userIdMocked)).thenReturn(Optional.of(userMocked));
		when(userRepository.save(userMocked)).thenReturn(userMocked);

		// when
		var realUser = userService.updateUser(userIdMocked, userDTO);

		// Assert
		assertTrue(realUser.isPresent());
		assertEquals(userMocked.getUserId(), realUser.get().getUserId());
		assertEquals(emailUpdated, realUser.get().getEmail());
		assertEquals(userMocked.getName(), realUser.get().getName());
		verify(userRepository, times(1)).save(userMocked);
	}

	@Test
	public void shouldUpdateUserNotExists() {
		// given
		var userIdMocked = "FAKE-ID";
		var userMocked = new User();
		userMocked.setUserId(userIdMocked);
		var emailUpdated = "EMAIL@NEW.COM";
		userMocked.setEmail(emailUpdated);

		var userDTO = new UserDTO();
		userDTO.setEmail(emailUpdated);

		when(userRepository.findById(userIdMocked)).thenReturn(Optional.empty());

		// when
		var realUser = userService.readUser(userIdMocked);

		// Assert
		assertTrue(realUser.isEmpty());
		verify(userRepository, times(0)).save(userMocked);
	}
	
	@Test
	public void shouldDeleteUserExists() {
		// given
		var userIdMocked = "FAKE-ID";
		var userMocked = mock(User.class);
		userMocked.setUserId(userIdMocked);
		when(userRepository.existsById(userIdMocked)).thenReturn(true);

		// when
		var realUserId = userService.deleteUser(userIdMocked);

		// Assert
		assertTrue(realUserId.isPresent());
		assertEquals(userIdMocked, realUserId.get());
		verify(userRepository, times(1)).deleteById(userIdMocked);
	}

	@Test
	public void shouldDeleteUserNotExists() {
		// given
		var userIdMocked = "FAKE-ID";
		var userMocked = mock(User.class);
		userMocked.setUserId(userIdMocked);
		when(userRepository.existsById(userIdMocked)).thenReturn(false);

		// when
		var realUser = userService.deleteUser(userIdMocked);

		// Assert
		assertTrue(realUser.isEmpty());
		verify(userRepository, times(0)).deleteById(userIdMocked);
	}
	
	@Test
	public void shouldReadUserWithCalculations() {
		// given
		var userIdMocked = "FAKE-ID";
		var calculation = new Calculation();
		calculation.setUserId(userIdMocked);
		when(calculationRepository.findAllByUserId(userIdMocked)).thenReturn(Arrays.asList(calculation));
		
		// when
		var userCalculations = userService.readCalculationsByUser(userIdMocked);
		
		// Assert
		assertFalse(userCalculations.isEmpty());
		userCalculations.forEach(c -> assertEquals(c.getUserId(), userIdMocked));
	}
	
	@Test
	public void shouldReadUserWithoutCalculations() {
		// given
		var userIdMocked = "FAKE-ID";
		var calculation = new Calculation();
		calculation.setUserId(userIdMocked);
		when(calculationRepository.findAllByUserId(userIdMocked)).thenReturn(Arrays.asList());
		
		// when
		var userCalculations = userService.readCalculationsByUser(userIdMocked);
		
		// Assert
		assertTrue(userCalculations.isEmpty());
	}


}
